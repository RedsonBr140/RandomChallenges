#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <json-c/json.h>
char token[34];
char alttoken[34];
void randomv(char var[34], time_t seed){
  char chars[35] = "0123456789abcdefghiklmnopqrstuvwxyz";
  int i;
  srand(seed);
  for (i = 0; i < 34; i++) {
    int rand_num = (rand() % (0 - 33 + 1) + 0);
    var[i] = chars[rand_num];
  }
}

int main(){
  char name[18];
  printf("Account name: "); scanf("%s", name);
   
  json_object *jobj = json_object_new_object();
  json_object *accobj = json_object_new_object();
  json_object *accounts = json_object_new_array();
  json_object *active = json_object_new_boolean(1);
  json_object *entitlement = json_object_new_object();
  json_object *owns = json_object_new_boolean(1);
  json_object *canplay = json_object_new_boolean(1);

  json_object *profile = json_object_new_object();
  json_object *capes = json_object_new_array();
  randomv(token, time(0));
  json_object *id = json_object_new_string(token);
  json_object *jname = json_object_new_string(name);
  json_object *skin = json_object_new_object();
  json_object *sskin = json_object_new_string("");

  json_object *type = json_object_new_string("Offline");
  json_object *ygg = json_object_new_object();
  randomv(alttoken, time(0)*10);
  json_object *altoken = json_object_new_string(alttoken);
  json_object *iat = json_object_new_int(0);
  json_object *ytk = json_object_new_string("offline");
  json_object *extra = json_object_new_object();
  json_object *formatVer = json_object_new_int(3);

  json_object_array_add(accounts, accobj);

  json_object_object_add(accobj, "active", active);
  json_object_object_add(accobj, "entitlement", entitlement);
  json_object_object_add(entitlement, "canPlayMinecraft", canplay);
  json_object_object_add(entitlement, "ownsMinecraft", owns);
  json_object_object_add(accobj, "profile", profile);
  json_object_object_add(profile, "capes", capes);
  json_object_object_add(profile, "id", id);
  json_object_object_add(profile, "name", jname);
  json_object_object_add(profile, "skin", skin);
  json_object_object_add(skin, "id", sskin);
  json_object_object_add(skin, "url", sskin);
  json_object_object_add(skin, "variant", sskin);
  json_object_object_add(jobj, "accounts", accounts);
  json_object_object_add(accobj, "type", type);
  json_object_object_add(accobj, "ygg", ygg);
  json_object_object_add(ygg, "extra", extra);
  json_object_object_add(extra, "clientName", jname);
  json_object_object_add(extra, "clientToken", altoken);
  json_object_object_add(ygg, "iat", iat);
  json_object_object_add(ygg, "token", ytk);
  json_object_object_add(jobj, "formatVersion", formatVer);
  printf("\njson:\n%s\n", json_object_to_json_string_ext(jobj, JSON_C_TO_STRING_PRETTY));
}
