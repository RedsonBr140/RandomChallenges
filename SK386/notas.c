#include <stdio.h>

float n1,n2,n3,n4;

float input(char *bin) {
  float nota;
  printf("Digite a nota do %s bimestre: ", bin);
  scanf("%f", &nota);
  return nota;
}

int main(){
  n1 = input("1°");
  n2 = input("2°");
  n3 = input("3°");
  n4 = input("4°");

  float nend = n1 + n2 + n3 + n4;
  if(nend >= 24) {
    printf("O aluno passou");
  } else if(nend < 24) {
    printf("O aluno foi reprovado");
  }
  printf("\npontuação total: %.1f\n", nend);
}
