# SK386
Essa é a pasta onde farei alguns dos desafios que o meu amigo [@SK386](https://github.com/SK386) me propôs(originalmente em Rust, agora em C).

## *`metas`*
 - [X] faça um hello world em C
 - [X] através de codicionais crie um sistema de aprovação de alunos, em um curso em que a média é 6.
 - [ ] crie um programa que use "switch" em C, o tema é loja de brinquedos, mostre os produtos,
permita que o usuario insira o valor de escolha do produto e mostre as caracteristicas do mesmo
